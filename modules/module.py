import os
import traceback

import string


class TemplateFormatter(string.Formatter):
	def format_field(self, value, spec):
		if spec.startswith("repeat"):
			template = spec.partition(":")[-1]
			if type(value) is dict:
				return ''.join([template.format(name=name, item=item) for name, item in value.items()])
			else:
				return ''.join([template.format(item=item) for item in value])
		elif spec == "call":
			return value()
		elif spec.startswith("if"):
			return (value and spec.partition(":")[-1]) or ""
		else:
			return super(TemplateFormatter, self).format_field(value, spec)

class Module:
	modules = {}
	
	def infos(self):
		print("infos")

	def init(self, id, window):
		self.id = id
		self.window = window
		self.links = {}
		self.tf = TemplateFormatter()
		self.log("Init")

	def setApp(self, app, webview):
		self.app = app
		self.webview = webview

	def setup(self):
		pass

	def getTemplate(self, name, **values):
		if os.path.isfile("data/templates/"+self.id+"/"+name+".html"):
			with open("data/templates/"+self.id+"/"+name+".html") as f:
				template = f.read()
				return self.tf.format(template, **values)
		return ""

	def getUsedZones(self):
		return ()

	def link(self, module, method, args=""):
		if not module in Module.modules:
			return
		module = Module.modules[module]
		if not method in module.links:
			module.links[method] = {}
		module.links[method][self.id] = args

	def triggerLinks(self, method):
		if not method in self.links:
			return
		for module, args in self.links[method].items():
			func = getattr(Module.modules[module], method, None)
			if func:
				func(args)

	def getCssFile(self):
		css_file = os.path.abspath(os.path.join("modules", self.id, "main.css"))
		if os.path.isfile(css_file):
			return "modules/%s/main.css" % self.id


	def eval(self, js):
		try:
			self.window.evaluate_js(js)
		except Error as e:
			self.error(js)
		self.log(js)

	def getE(self, name):
		e = self.window.get_elements(name)
		if e:
			return e[0]
		else:
			self.log("Dom Element not found : %s" % name)
			return ()

	def setE(self, element, html):
		js = r"""let div = document.getElementById("%s")
		if (div != undefined) {
			div.innerHTML = "%s"
		}
		 """ % (element, html.replace("\"", "\\\""))
		self.eval(js)
		

	def showE(self, element):
		js = r"""let div = document.getElementById("%s")
		if (div != undefined) {
			div.style.display = "block"
		}
		 """ % element
		self.eval(js)

	def hideE(self, element):
		js = r"""let div = document.getElementById("%s")
		if (div != undefined) {
			div.style.display = "none"
		}
		 """ % element
		self.eval(js)
		
	def setHtml(self, zone, html):
		self.log(zone+" = ["+html+"]")
		js = r"""let div = document.getElementById("%s-%s")
		console.log(div)
		if (div != undefined) {
			div.innerHTML = "%s"
		}
		 """ % (zone, self.id, html.replace("\"", "\\\""))
		self.eval(js)

	def addHtml(self, zone, html):
		if not html:
			return
		js = r"""let div = document.getElementById("%s-%s")
		if (div != undefined) {
			div.innerHTML = div.innerHTML+"%s"
		}
		 """ % (zone, self.id, html.replace("\"", "\\\""))
		self.eval(js)


	def log(self, text):
		print("[%s:%s]" % (self.id, traceback.format_stack()[-2].strip().split("\n")[0].split(" ")[-1]), text)
	
	def error(self, text):
		print("ERROR: [%s:%s]" % (self.id, traceback.format_stack()[-2].strip().split("\n")[0].split(" ")[-1]), text)

	def warning(self, text):
		print("WARN: [%s:%s]" % (self.id, traceback.format_stack()[-2].strip().split("\n")[0].split(" ")[-1]), text)

