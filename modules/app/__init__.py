import time
import threading

from ..module import Module

VERSION="0.7 (from scrap)"
AUTHOR="Ul'U Kyn"

class ScrapShare(Module):
	dom_ready = False

	def toggleFullscreen(self, param):
		webview.windows[0].toggle_fullscreen()

	def exit(self, item):
		self.window.destroy()
		for module in self.modules_threads:
			t.stop()

	def startApp(self, args=""):
		contents = {"header": {}, "main": {}, "popup": {}, "footer": {}}

		css = ""
		for name, module in Module.modules.items():
			css_module = self.modules[name].getCssFile()
			if css_module:
				css += "<link href=\"%s\" rel=\"stylesheet\">\n" % self.modules[name].getCssFile()
			zones = module.getUsedZones()
			for zone in contents:
				if zone in zones:
					zone_prio = zones[zone]
					if not zone_prio in contents[zone]:
						contents[zone][zone_prio] = []
					contents[zone][zone_prio].append("<div id=\""+zone+"-"+name+"\"></div>")

		zones_html = {}
		for zone in contents:
			zones_html[zone] = ""
			sorted_zones = sorted(contents[zone])
			for s_zone in sorted_zones:
				htmls = contents[zone][s_zone]
				for html in htmls:
					zones_html[zone] += html

		# Fill Templates
		with open("data/header.html") as f:
			header_html = f.read().replace("{{modules}}", zones_html["header"])

		with open("data/footer.html") as f:
			footer_html = f.read().replace("{{modules}}", zones_html["footer"])
			footer_html = footer_html.replace("{{version}}", VERSION)
			footer_html = footer_html.replace("{{author}}", AUTHOR)

		with open("data/index.html") as f:
			html = f.read()
		html = html.replace("{{header}}", header_html)
		html = html.replace("{{footer}}", footer_html)
		html = html.replace("{{main}}", zones_html["main"])
		html = html.replace("{{popup}}", zones_html["popup"])
		html = html.replace("{{css}}", css)

		self.log(html)
		
		self.window.load_html(html)
		print("Waiting for dom ready...")
		while not self.dom_ready:
			time.sleep(0.1)

		print("Dom ready !")
		self.modules_threads = {}
		for module in Module.modules:
			print("Start setuping %s..." % module)
			t = threading.Thread(target=Module.modules[module].setup)
			self.modules_threads[module] = t
			t.start()

	def onLoaded(self):
		print("Dom Ready")
		self.dom_ready = True
		self.window.loaded -= self.onLoaded

instance = ScrapShare()