import os
import sys
import pkgutil

from ..module import Module

from pathlib import Path
from base64 import b64decode
from importlib import import_module
from Crypto.Hash import SHA256
from Crypto.Signature import PKCS1_v1_5
from Crypto.PublicKey import RSA

class Api(Module):

	def init(self, webview, window):
		self.id = "api"
		self.links = {}
		self.webview = webview
		self.window = window
		with open("data/pub.pem", "rb") as f:
			self.key = RSA.importKey(f.read())
		self.key_verifier = PKCS1_v1_5.new(self.key)
		self.log("Init")


	def loadModules(self):
		Module.modules = {"api": self}
		# Load the main module : app
		module = import_module("modules.app", package=__name__)
		self.app = module.instance
		self.app.init("app", self.window)
		self.app.setApp(self.app, self.webview)
		Module.modules["app"] = self.app
		
		for (_, name, _) in pkgutil.iter_modules([os.path.join(Path(__file__).parent.parent.parent, "modules/")]):
			self.log(name)
			if name != "module" and name != "api"  and name != "app":
				print("Load module: %s" % name)
				module = import_module("modules."+name, package=__name__)
				if hasattr(module, "instance"):
					Module.modules[name] = module.instance
					Module.modules[name].init(name, self.window)
					Module.modules[name].setApp(self.app, self.webview)

	def getModule(self, name):
		if name in Module.modules:
			return Module.modules[name]
		return None

	def run(self, signature, module, method, args=""):
		url = self.app.window.get_current_url()
		
		if url[:8] != "file:///" :
			print(signature)
			sign = b64decode(signature)
			digest = SHA256.new()
			digest.update((module+":"+method+":"+args).encode())
			verified = self.key_verifier.verify(digest, sign)
			assert verified, "Signature verification failed"
			print("Successfully verified message")
		else:
			args = method
			method = module
			module = signature
		if module in Module.modules:
			func = getattr(Module.modules[module], "call_"+method, None)
			if func:
				try:
					func(args)
					Module.modules[module].triggerLinks(method)
				except Exception as e:
					print(traceback.print_exc())
			else:
				print("ERROR: no method call_%s in module %s found" % (method, module))
		else:
			print("ERROR: no module found : %s" % module)


