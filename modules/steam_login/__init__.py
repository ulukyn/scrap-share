from ..module import Module
import os, platform, glob
import uuid, json
import urllib.request
from os import path as p

class SteamLogin(Module):
	logged = False

	def getUsedZones(self):
		return {"main": 1, "header": 1}

	def setup(self):
		return self.displayMain()

	def displayMain(self, args=""):
		if not os.path.isfile(".scrap_share_id"):
			scrap_share_id = str(uuid.uuid1())
			with open(".scrap_share_id", "w") as f:
				f.write(scrap_share_id)
		else:
			with open(".scrap_share_id", "r") as f:
				scrap_share_id = f.read()

		# Try to get informations about steam account
		with urllib.request.urlopen("https://cloud.troispetits.net:8080/authenticate_steam_user.php?get_infos=steam&scrap_share_id="+scrap_share_id) as response:
			try:
				self.infos = json.loads(response.read())
			except Exception as e:
				print(e)
				self.infos = False

		if self.infos:
			self.scrap_share_id = scrap_share_id
			self.triggerLinks("callUserLogged")
			print(self.infos)
			self.setHtml("header", "<span style='font-weight: bold'>"+self.infos["personaname"]+"</span> <img src='"+self.infos["avatar"]+"' />")
		else:
			html = "<table width='100%' cellspacing='5'>"
			html += "<tr><td align='center'><h3>Merci de vous connectez sur Steam en suivant ce lien:</h3></td></tr>"
			html += "<tr><td align='center'><a href='https://cloud.troispetits.net:8080/authenticate_steam_user.php?scrap_share_id="+scrap_share_id+"'><img src='https://steamcommunity-a.akamaihd.net/public/images/signinthroughsteam/sits_02.png'></td></tr>"
			html += "</table>"
			self.setHtml("main", html)


instance = SteamLogin()
