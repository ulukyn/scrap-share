from ..module import Module
import os, urllib3, certifi, json, re
import hashlib

def sizeof_fmt(num, suffix="o"):
	for unit in ["","K","M","G","T","P","E","Z"]:
		if abs(num) < 1024.0:
			return "%3.1f%s%s" % (num, unit, suffix)
		num /= 1024.0
	return "%.1f%s%s" % (num, "Y", suffix)

class ScrapApp(Module):

	def getUsedZones(self):
		return {"main": 2, "footer": 1, "popup": 1}

	def setup(self):
		self.link("steam_login", "callUserLogged")
		self.http = urllib3.PoolManager(cert_reqs="CERT_REQUIRED",  ca_certs=certifi.where())
		return ""

	def openFileFialog(self):
		file_types = ("Scrap Game save files (*.db)",)
		return self.window.create_file_dialog(self.webview.OPEN_DIALOG, allow_multiple=False, file_types=file_types)
	
	def _getScrapName(self, filename):
		return " ".join(re.findall("[A-Z][^A-Z]*", filename[:-3]))

	def callUserLogged(self, args=""):
		steam_id = self.app.modules["steam_login"].infos["steamid"]
		self.scrap_share_id = self.app.modules["steam_login"].scrap_share_id
		scrap_dir = "/Axolot Games/Scrap Mechanic/User/User_"+steam_id+"/Save/Survival/"
		scrap_path = ""
		if (os.getenv("APPDATA")):
			scrap_path = os.getenv("APPDATA")+scrap_dir

		if not scrap_path or not os.path.isdir(scrap_path):
			if os.path.isfile(".scrap_backup_path"):
				with open(".scrap_backup_path", "r") as f:
					scrap_path = f.read()
			else:
				db_file = self.openFileFialog()
				print(scrap_dir, db_file)
				if db_file and scrap_dir in db_file[0]:
					scrap_path = os.path.dirname(db_file[0])
					with open(".scrap_backup_path", "w") as f:
						f.write(scrap_path)

		if scrap_path:
			html = "Le dossier de sauvegarde de Scrap a été trouvé !"
			self.setHtml("footer", html)
			
			r = self.http.request("GET", "https://cloud.troispetits.net:8080/scrap_files.php?scrap_share_id="+self.scrap_share_id)
			try:
				remote_files = json.loads(r.data)
			except Exception as e:
				print(e)
				remote_files = {}
				
			print("remotes", remote_files)
				
			html = "<table width='100%' style='padding: 5px' cellspacing='0' cellpadding='3'><tr style='background-color:orange; font-weight: bold; color: black'><td height='32px'>Vos parties</td><td></td><td></td><td align='right'>Partages</td><td width='150px' align='center'>Cloud</td></tr>"
			self.scrap_path = scrap_path
			try:
				with open(".scrap_files_versions", "r") as f:
					self.file_versions = json.loads(f.read())
			except Exception as e:
				self.file_version = {}
				
			for scrap_file in os.listdir(scrap_path):
				if scrap_file in self.file_version:
					version = int(scrap_file[file_version])
				else:
					version = 1
	
				upload = "<a href='#' onclick='pywebview.api.run(\"scrap_app\", \"uploadFirstTime\", \""+scrap_file+"\")'><img src='data/upload.png' /></a>"
				remove = "<a href='#' onclick='pywebview.api.run(\"scrap_app\", \"removeFile\", \""+scrap_file+"\")'><img src='data/remove.png' /></a>"
				if scrap_file in remote_files:
					share_status = remote_files[scrap_file]["share"]
					if not share_status: share_status = ""
					cloud_version = int(remote_files[scrap_file]["version"])
					if not cloud_version: cloud_version = 0
					
					if version == cloud_version:
						cloud_status = remove
					elif version > cloud_version:
						cloud_status = "En retard..."
					else:
						cloud_status = "En avance..."
				
				if not cloud_version:
					cloud_status = upload
				name = self._getScrapName(scrap_file)
				share = "<a href='#' onclick='pywebview.api.run(\"scrap_app\", \"shareFile\", \""+scrap_file+"\")'><img src='data/add.png' /></a>"
				html += "<tr><td>%s</td><td>v%d</td><td>%s</td><td height='32px' align='right' title='add'>%s</td><td align='center'>v%d %s</td></tr>" % (name, version, sizeof_fmt(os.stat(scrap_path+"/"+scrap_file).st_size), share, cloud_version, cloud_status)
			html += "</table>"
			self.setHtml("main", html)
		else:
			html = "<table width='100%' cellspacing='5'><tr><td align='center' bgcolor='red'><h3>Le dossier de sauvegarde de Scrap n'a pas été trouvé :'(</h3></td></tr></table>"
			self.setHtml("main", html)

	def call_uploadFirstTime(self, filename):
		if filename in self.file_version:
			version = int(scrap_file[filename])
		else:
			version = 1
		
		m = hashlib.sha1()
		with open(self.scrap_path+"/"+filename, "rb") as f:
			content = f.read()
			m.update(content)
			headers = {"content-type": "application/x-www-form-urlencoded"}
			r = self.http.request("POST", "https://cloud.troispetits.net:8080/scrap_upload_file.php?scrap_share_id="+self.scrap_share_id+"&version="+str(version)+"&hash="+m.hexdigest(), fields={"filefield": (filename, content)})
			print(r.data)
			try:
				status = json.loads(r.data)
			except Exception as e:
				print(e)
				status = False
			print(status)
			
			if status:
				self.setHtml("footer", "Upload réussi")
			else:
				self.setHtml("footer", "Problème avec l'upload")
			self.callUserLogged()

	def call_removeFile(self, filename):
		self.http.request("POST", "https://cloud.troispetits.net:8080/scrap_remove_file.php?scrap_share_id="+self.scrap_share_id+"&scrap_file="+filename)
		self.callUserLogged()
	
	def shareFile(self, filename):
		# TODO : get shares
		users = {"123" : {"name": "OOX", "avatar" : "data/upload.png"}, "314" : {"name": "Samy", "avatar" : "data/upload.png"}}
		html = "<table width='100%'>"
		html += "<tr><td align='center'><h3>Partager la partie <span style='color: orange'>"+self._getScrapName(filename)+"</span> avec</h3></td></tr>"
		for steam_id, user in users.items():
			html += "<tr><td>"+"<img src='"+user["avatar"]+"'/> "+user["name"]+"</td></tr>"
		html += "</table>"
		
		html = self.getTemplate("sharing_list", filename=self._getScrapName(filename), users=users)
		print("html", html)
		self.setHtml("popup", html)
		self.showE("popup")
		

instance = ScrapApp()
