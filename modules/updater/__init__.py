import os
import sys
import pkgutil
import gitlab
import base64
from pathlib import Path

from ..module import Module

class Updater(Module):

	def _updateModule(self, project, name):
		for item in items:
			if item["name"] == "__init__.py":
				file_info = project.repository_blob(item["id"])
				content = base64.b64decode(file_info["content"])
				with open(name+"/__init__.py", "w") as f:
					f.write(content)

	def updateModules(self):
		gl = gitlab.Gitlab("https://gitlab.com")
		project = gl.projects.get(14257723)
		items = project.repository_tree(path="modules", ref="master", recursive=True)
		print(items)

		for (_, name, _) in pkgutil.iter_modules([os.path.join(Path(__file__).parent.parent.parent, "modules/")]):
			print("Updating module: %s" % name)
			self._updateModule(project, name)
